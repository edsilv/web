export default interface IVResponse {
    status: number;
    statusText: string;
    data: any;
    headers: {[header: string]: string};
    ok: boolean;
// eslint-disable-next-line semi
}
