import IVResponse from './IVResponse';

export interface IFetchErrorParams {
    networkError?: any;
    url?: string;
    init?: any;
    response?: IVResponse
}

/**
 * Error to be returned on Fetch request failure/errors;
 */
export class FetchError extends Error {
    public networkError?: any;

    public url?: string;

    public init?: any;

    public response?: IVResponse;

    constructor(
        { url, init, networkError, response }: IFetchErrorParams,
        ...params: any[]
    ) {
        super(...params);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, FetchError);
        }

        this.name = 'FetchError';
        this.url = url;
        this.init = init;
        this.networkError = networkError;
        this.response = response;

        if (this.url) {
            // meant for stack-trace.. for user presentation, create custom message by using above components
            if (this.networkError) {
                this.message = `network error while fetching ${this.url}\n\nerror: ${String(networkError)}`;
            } else {
                this.message = `error while fetching ${this.url}\n\nresponse: ${JSON.stringify(this.response, undefined, 2)}`;
            }
        }
    }
}
