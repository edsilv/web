import { FetchError } from './FetchError';
import IVResponse from './IVResponse';
import { fetch, URL, URLSearchParams, Headers, Request, Response, FormData } from './apis';


const JSON_MIME_REGEX = new RegExp('^application/(.*\\\+)?json');
const XML_MIME_REGEX = new RegExp('^application/(.*\\\+)?xml');

/**
 * parses response body according to Content-Type and retuns promise of unified, bodyParsed response for both response.OK and otherwize
 * @param response fetch Response
 */
async function parseBody(response: any) {
    const { headers } = response;
    const contentType = headers.get('Content-Type') as string;
    let bodyType = 'blob';
    if (!contentType) {
        bodyType = 'blob';
    } else if (contentType.startsWith('text/') || XML_MIME_REGEX.test(contentType)) {
        bodyType = 'text';
    } else if (JSON_MIME_REGEX.test(contentType)) {
        bodyType = 'json';
    }

    // @ts-ignore
    const dataPromise = response[bodyType]();
    const data = await dataPromise;
    return data;
}

/**
 * interface for using fetch, with error handling
 * @param url - URL of resources
 * @param init  - fetch Request init
 * @param ifetch - fetch implementation to use. if not given, fallbacks on fetch availble in environment. i.e. fetch/node-fetch for browser/node respectively. useful when some bounded fetch have to be used, like in sapper routes.
 */
export async function request(url: string, init?: RequestInit, ifetch = fetch): Promise<IVResponse> {
    let response;
    try {
        response = await ifetch(url, init);
    } catch (e) {
        throw new FetchError({
            url,
            networkError: { message: e.message },
        });
    }

    const data = await parseBody(response);
    const pojoHeaders: {[header: string]: string} = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const k of response.headers.keys()) {
        pojoHeaders[k] = response.headers.get(k);
    }

    const mResponse = {
        status: response.status,
        statusText: response.statusText,
        data,
        headers: pojoHeaders,
        ok: response.ok,
    };

    if (!mResponse.ok) {
        throw new FetchError({
            url,
            response: mResponse,
        });
    }
    return mResponse;
}

export { FetchError, fetch, URL, URLSearchParams, Headers, Request, Response, FormData, IVResponse };
