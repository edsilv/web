/* eslint-disable global-require */
/**
 * central place for node&browser web api normalization
 */

// eslint-disable-next-line no-new-func
const isBrowser = new Function('try {return this===window;}catch(e){ return false;}')();

let FetchCookieWrapper;
let nodeFetch;
let url;
let nodeFormData;
if (!isBrowser) {
    FetchCookieWrapper = require('fetch-cookie/node-fetch');
    nodeFetch = require('node-fetch');
    url = require('url');
    nodeFormData = require('form-data');
}

const isoFetch = (isBrowser ? fetch : FetchCookieWrapper(nodeFetch));
// @ts-ignore
const isoHeaders = isBrowser ? Headers : nodeFetch.Headers;
// @ts-ignore
const isoRequest = isBrowser ? Request : nodeFetch.Request;
// @ts-ignore
const isoResponse = isBrowser ? Response : nodeFetch.Response;
const isoFormData = (isBrowser ? FormData : nodeFormData) as any;
const isoURL = isBrowser ? URL : url.URL;
const isoURLSearchParams = isBrowser ? URLSearchParams : url.URLSearchParams;

export {
    isoFetch as fetch,
    isoHeaders as Headers,
    isoRequest as Request,
    isoResponse as Response,
    isoFormData as FormData,
    isoURL as URL,
    isoURLSearchParams as URLSearchParams,
};
